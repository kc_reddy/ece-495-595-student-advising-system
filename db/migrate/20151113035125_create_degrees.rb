class CreateDegrees < ActiveRecord::Migration
  def change
    create_table :degrees do |t|
      t.string :name
      t.string :college
      t.string :program_path

      t.timestamps null: false
    end
  end
end
