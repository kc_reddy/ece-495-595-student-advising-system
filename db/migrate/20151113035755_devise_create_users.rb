class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      
      t.string     :username,               null: false
      t.references :degree, :major,         foreign_key: true
      t.references :degree, :minor,         foreign_key: true
      t.string     :email,                  null: :false, default: :""
      t.integer    :sign_in_count,          default: 0
      t.datetime   :current_sign_in_at
      t.datetime   :last_sign_in_at
      t.string     :current_sign_in_ip
      t.string     :last_sign_in_ip
      
      t.timestamps null: false

    end
    
    add_index :users, :username,            unique: true
    
  end
end
