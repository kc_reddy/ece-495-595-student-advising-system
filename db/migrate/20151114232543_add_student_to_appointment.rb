class AddStudentToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :student_id, :integer, references: :users
  end
end
