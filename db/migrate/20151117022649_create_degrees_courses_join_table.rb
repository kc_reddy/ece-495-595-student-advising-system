class CreateDegreesCoursesJoinTable < ActiveRecord::Migration
  def change
    create_join_table :degrees, :courses do |t|
      # t.index [:degree_id, :course_id]
      # t.index [:course_id, :degree_id]
    end
  end
end
