# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151117022649) do

  create_table "appointments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "note_id"
    t.datetime "start_time", null: false
    t.datetime "end_time",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "student_id"
  end

  add_index "appointments", ["note_id"], name: "index_appointments_on_note_id"
  add_index "appointments", ["user_id"], name: "index_appointments_on_user_id"

  create_table "courses", force: :cascade do |t|
    t.string   "title"
    t.string   "subject"
    t.integer  "credit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "courses_degrees", id: false, force: :cascade do |t|
    t.integer "degree_id", null: false
    t.integer "course_id", null: false
  end

  create_table "degrees", force: :cascade do |t|
    t.string   "name"
    t.string   "college"
    t.string   "program_path"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "notes", force: :cascade do |t|
    t.text     "advisor_notes"
    t.text     "advisor_student_notes"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles_users", id: false, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "role_id", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "username",                        null: false
    t.integer  "degree_id"
    t.integer  "major_id"
    t.integer  "minor_id"
    t.string   "email",              default: ""
    t.integer  "sign_in_count",      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "users", ["username"], name: "index_users_on_username", unique: true

end
