class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @appointments = current_user.appointments
  end


  def show
  end


  def new
    @appointment = current_user.appointments.new
  end


  def edit
  end


  def create
    @appointment = current_user.appointments.new(appointment_params)
    @current_user = current_user
    advisor_name = @appointment[:user_id]
    puts advisor_name
    @advisor = User.get_advisors.find_by_username(advisor_name)
    respond_to do |format|
      if @appointment.save
        #UserMailer.send_appointment_email(@appointment,@current_user).deliver_now
        UserMailer.meeting_request_with_calendar(@current_user,@appointment).deliver_now
        #UserMailer.add_interaction(@current_user).deliver_now
        #format.html { redirect_to current_user, notice: 'Successfully created appointment' }
        format.html { redirect_to current_user, notice: advisor_name }
        format.json { render :show, status: :created, location: @appointment }
      else
        format.html { render :new }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    respond_to do |format|
      if @appointment.update(appointment_params)
        format.html { redirect_to current_user, notice: 'Appointment was successfully updated.' }
        format.json { render :show, status: :ok, location: @appointment }
      else
        format.html { render :edit }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @appointment.destroy
    respond_to do |format|
      format.html { redirect_to appointments_url, notice: 'Appointment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appointment
      @appointment = Appointment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def appointment_params
      params.require(:appointment).permit(:user, :student, :start_time, :end_time, :note)
    end
end
