class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :cas_authenticatable, :trackable
  has_and_belongs_to_many :roles
  has_many :appointments

  validates :username, presence: true
  validates :email, presence: true
  validates :role_ids, presence: true


  def self.get_advisors
    role = Role.find_by(:name =>"Advisor")
    # User.where(role_id: role.id).all
    User.joins(:roles).where("roles_users.role_id is #{role.id}")
  end
  

  # Get open times. Queries the appointments table to retrieve
  # time slots available for a given user Assume you want times from 8-5 M-F.
  # 
  # Returns a list of times in the form of Time objects (for now).
  #
  def self.get_availablities(advisor_id, start_t, end_t)

    if Appointment.find_by(user_id=advisor_id).nil?
      "free"
    else
      Appointment.find_by(user_id=advisor_id)
      # [Time.utc(2015,11,17,8,15)]
    end
  end

end
