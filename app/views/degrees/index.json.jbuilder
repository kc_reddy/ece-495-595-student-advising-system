json.array!(@degrees) do |degree|
  json.extract! degree, :id, :name, :document, :college
  json.url degree_url(degree, format: :json)
end
