class UserMailer < ApplicationMailer
  default :from => 'any_from_address@example.com'
  require 'icalendar'

  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_signup_email(user)
    @user = user
    mail( :to => @user.email,
    :subject => 'Thanks for signing up for our amazing app' )
  end

    def send_appointment_email(appointment,user)
    @appointment = appointment
    @user = user
    mail( :to => @user.email,
    #'chaitanya.ks47@gmail.com',
    :subject => 'You have an appointment scheduled'  )
  end

  #  def meeting_request_with_calendar(user)
  #  	@user = user
  #    mail(:to => @user.email, :subject => "iCalendar",
  #                 :from => "krish.reddy91@gmail.com") do |format|
  #      format.ics {
  #      ical = Icalendar::Calendar.new
  #      e = Icalendar::Event.new
  #      e.start = DateTime.now.utc
  #      e.start.icalendar_tzid="UTC" # set timezone as "UTC"
  #      e.end = (DateTime.now + 1.day).utc
  #      e.end.icalendar_tzid="UTC"
  #      e.organizer "any_email@example.com"
  #      e.uid "MeetingRequest#{234}"
  #      e.summary "Scrum Meeting"
  #      e.description <<-EOF
  #        Venue: Office
  #        Date: 16 August 2011
  #        Time: 10 am
  #      EOF
  #      ical.add_event(e)
  #      i cal.publish
  #      ical.to_ical
  #      render :text => ical, :layout => false
  #     }
  #   end
  # end

  # Create a calendar with an event (standard method)
  
 #  def meeting_request_with_calendar(user)
 #    	@user = user
 #    	mail(:to => @user.email, :subject => "iCalendar",
 #                  :from => "krish.reddy91@gmail.com") do |format|
 #       format.ics {
	# 	cal = Icalendar::Calendar.new

	#     cal.event do |e|
	# 	  e.dtstart     = Icalendar::Values::Date.new('20151205')
	# 	  e.dtend       = Icalendar::Values::Date.new('20151205')
	# 	  e.summary     = "Meeting with the man."
	# 	  e.description = "Have a long lunch meeting and decide nothing..."
	# 	  e.ip_class    = "PRIVATE"
	# 	end

	# 	cal.publish
	#     cal.to_ical
 #       render :text => cal, :layout => false
 #      }

 #   		end
	# end

 def meeting_request_with_calendar(user,appointment)
    	@user = user
    	@appointment = appointment
    	#@advisor = advisor
		cal = Icalendar::Calendar.new
		e = Icalendar::Event.new
		  e.dtstart   = @appointment.start_time
		  e.dtend     = @appointment.end_time
  		  e.organizer = Icalendar::Values::CalAddress.new("mailto:advisor@example.com", cn: 'Advisor_name')
  		  e.attendee  =  Icalendar::Values::CalAddress.new("mailto:student@example.com", cn: @user.username)		  

		  e.summary     = "Advisement Meeting"
		  e.description = "Short Meeting to discuss on courses"
		  e.ip_class    = "PRIVATE"
		  e.location    = "Dane Smith Hall, Room 215"
		
		cal.add_event(e)
		
		email=mail(to: @user.email, subject: "Advisement",mime_version: "1.0", content_type:"text/calendar",
    				body:cal.to_ical,content_disposition:"inline; filename=calendar.ics", filename:'calendar.ics')
    	
    	#email.header=email.header.to_s +'Content-Class:urn: content-classes:calendarmessage'
    	return email    		
    end


def add_interaction(user)
    @user = user
    ical = Icalendar::Calendar.new
    e = Icalendar::Event.new
    e.start = DateTime.now.utc
    e.start.icalendar_tzid="UTC" # set timezone as "UTC"
    e.end = (DateTime.now + 1.hour).utc
    e.end.icalendar_tzid="UTC"
    e.organizer @user.email
    e.created = DateTime.now
    e.uid = e.url = "#{HOSTNAME.to_s+'/interaction/'+@user.id.to_s}"
    e.summary "fjdndfglkdflgj"
    e.description <<-EOF
    #{INTERACTION_TYPE[@i.itype][0]} with #{@i.company}
    Date: #{@i.start_time.strftime('%e %b %Y')}
    Time: #{@i.start_time.strftime('%I:%M %p')}
    EOF
    ical.add_event(e)
    ical.custom_property("METHOD", "REQUEST")

    email=mail(to: @user.email, subject: "Interview",mime_version: "1.0", content_type:"text/calendar",
    	body:ical.to_ical,content_disposition:"inline; filename=calendar.ics", filename:'calendar.ics')
    email.header=email.header.to_s +'Content-Class:urn: content-classes:calendarmessage'
    return email
  end


end
