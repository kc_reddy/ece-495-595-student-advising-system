Rails.application.routes.draw do
  get 'pages/index'

  resources :notes
  
  devise_for :users
  resources :courses
  resources :degrees
  # resources :appointments

  resources :users#, :except => [:create, :new]

   resources :users do                                                                                                                                                           
     resources :appointments
   end
 
  root to: "pages#index"
 
end
